import { Injectable } from '@nestjs/common';
import { IJobDTO } from '../dto/jobcron/job.dto';

@Injectable()
export abstract class IJobService {

    abstract update(message: String): Promise<any>;
    abstract getHour(): Promise<any>;
}