import { Injectable } from "@nestjs/common";
import { ICategoriesService } from '../categories.service';
import { ICategorie } from '../../../core/entity/categorie.entity';
import { ICategoriesUC } from '../../../core/use-case/categories.uc';
import { ResponseService } from "src/controller/dto/response-job.dto";
import { IJobUc } from '../../../core/use-case/job.uc';



@Injectable()
export class CategoriesService implements ICategoriesService {
    constructor(private readonly iCategoriesUc: ICategoriesUC) { }

    async getCategories(): Promise<ResponseService<ICategorie[]>> {

        try {
            
            return this.iCategoriesUc.getCategories();
        } catch (error) {
            throw error;
        }


    }

}