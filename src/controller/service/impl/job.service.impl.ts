import { Injectable } from '@nestjs/common';
import { ResponseService } from 'src/controller/dto/response-job.dto';
import { EmessageMapping } from 'src/common/utils/enums/message.enum';
import { IJobService } from '../job.service';
import { IJobDTO } from 'src/controller/dto/jobcron/job.dto';
import { IJob } from 'src/core/entity/job.entity';
import { IJobUc } from 'src/core/use-case/job.uc';
import { JobUcimpl } from 'src/core/use-case/impl/job.uc.impl';

@Injectable()
export class JobService implements IJobService {
  constructor(private readonly _jobUC: IJobUc) { }


  async update(id: string): Promise<ResponseService<any>> {
    const result = await this._jobUC.update(id);
    return new ResponseService(
      true,
      'Execution successful',
      200,
    );
  }

  async getHour(): Promise<ResponseService<any>> {
    const result = await this._jobUC.getHour();
    return new ResponseService(
      true,
      'Execution successful',
      200,
      result,
    );


  }

  public static mappingMessage(idMessage: EmessageMapping): string {
    return JobUcimpl.getMessages.find(m => m.id == idMessage)?.message;
  }
}