import { Injectable } from "@nestjs/common";
import { ICategorie } from '../../core/entity/categorie.entity';
import { ResponseService } from "../dto/response-job.dto";




@Injectable()
export abstract class ICategoriesService {
    abstract getCategories(): Promise<ResponseService<ICategorie[]>>;
}