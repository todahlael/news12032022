import { Controller, Get } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import generalConfig from 'src/common/configuration/general.config';
import { ICategoriesService } from './service/categories.service';
import { IJobUc } from '../core/use-case/job.uc';


@Controller(`${generalConfig.apiVersion}${generalConfig.controllerCategories}`)



export class CategoriesController {


    constructor(private readonly iCategoriesService: ICategoriesService,
        private readonly iJobUc: IJobUc) {
    }

    // @Cron(CronExpression.EVERY_MINUTE)
    // async getHour() {

    //     let hour = await this.iJobUc.getHourCron();
    //     let hourSplit = hour.split(' ');
    //     //console.log('HORA=>', hourSplit);
    //     let dateNow = new Date();
    //     if (parseInt(hourSplit[0]) === dateNow.getMinutes() && parseInt(hourSplit[1]) === dateNow.getHours()) {
    //         return this.iCategoriesService.getCategories();
    //     }
    // }
//este es el cron que me pidieron colocar.
    @Cron(CronExpression.EVERY_HOUR)
    async getHour() {
        console.log("esta aplicacion se ejecutara cada 60 minutos o cada hora")
        let  readNews = this.iCategoriesService.getCategories();
        //console.log("READNEWSSSSSSSSSSSSS",readNews)
    
    }




}

