import { Module } from '@nestjs/common';
import { CoreModule } from 'src/core/core.module';
import { JobController } from './job.controller';
import { IJobService } from './service/job.service';
import { JobService } from './service/impl/job.service.impl';
import { DataProviderModule } from 'src/data-provider/data-provider.module';
import { CategoriesController } from './categories.controller';
import { ICategoriesService } from './service/categories.service';
import { CategoriesService } from './service/impl/categories.service.impl';
import { ScheduleModule } from '@nestjs/schedule';


@Module({
  imports: [
    CoreModule,
    DataProviderModule,
    ScheduleModule.forRoot()
  ],
  controllers: [JobController, CategoriesController],
  providers: [

    { provide: IJobService, useClass: JobService },
    { provide: ICategoriesService, useClass: CategoriesService }

  ],
})
export class ControllerModule { }
