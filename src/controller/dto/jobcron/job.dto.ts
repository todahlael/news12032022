import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, Max, Min } from "class-validator";
import { max } from "rxjs";

export class IJobDTO {
    
    
    @ApiProperty()
    @IsNotEmpty()
    @Min(0)
    @Max(23)
    readonly hour: number;

    @Min(0)
    @Max(59)
    @ApiProperty()
    @IsNotEmpty()
    readonly minute: number;


 
}