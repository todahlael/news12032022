import { BadRequestException, Body, Controller, Get, Param, ParseIntPipe, Post, Put, Query, } from '@nestjs/common';
import generalConfig from 'src/common/configuration/general.config';
import { MappingApiRest, MethodMessage } from 'src/common/utils/enums/mapping-api-rest';
import GeneralUtil from 'src/common/utils/utils';
import { IJobDTO } from './dto/jobcron/job.dto';
import { IJobService } from './service/job.service';
const { logger, logOutput, levelsErros } = require('@claro/logging-library');
const info = require('../../package.json');
const rTracer = require('cls-rtracer')

@Controller(`${generalConfig.apiVersion}${generalConfig.controllerMockup}`)
export class JobController {

  constructor(private readonly _jobService: IJobService) { }


  @Put('/set/:Id')
  update(@Param('Id') id: string) {
    //console.log("aqui recibio el ID EN EL CONTROLLER", id)
    return this._jobService.update(id);
  }

  @Get('/shedule')
  getHour() {
    return this._jobService.getHour();
  }
}
