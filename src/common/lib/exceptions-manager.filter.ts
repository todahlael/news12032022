import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from "@nestjs/common";
import { Request, Response } from 'express';
import * as moment from 'moment';
import { ResponseService } from "../../controller/dto/response-job.dto";
import { EmessageMapping } from "../utils/enums/message.enum";
import GeneralUtil from "../utils/utils";
import { BusinessException } from './business-exceptions';


@Catch()
export class ExceptionManager implements ExceptionFilter {
  // ...
  async catch(exception, host: ArgumentsHost) {


    //bueno idea que tengo es coger esta cuestion y parametrizarla con el exception
    //como esta parametrizado.


    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const req = ctx.getRequest();

    let result: ResponseService;

    if (exception instanceof BusinessException) {
      console.log("aqui entro a businnesexceptio")
      console.error('exception => ', exception);
      result = new ResponseService(true, exception?.details?.codMessage, exception.code);
    }
    else if (exception instanceof HttpException) {
      result = new ResponseService(true, exception.name.replace('Exception', ''), exception.getStatus());
      console.log("aqui va la excepcion de la instancia http exception", exception.name)
    }
    else {
      console.log("entro al internal server error")
      console.error('exception => ', exception);
      result = new ResponseService(true, exception?.details?.codMessage, exception.code);

      //result = new ResponseService(true, EmessageMapping.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    const origen: string = "MSAbCaAlarFeaturesETL";

    console.error('exception result invetigar => ', result);

    //aqui va el resultado del error
    //console.log("aqui va el resultado del error", result.status)


    response.status(result.status).json({
      ...result,
      requestTime: moment().format(),
      // method: req.method,
      origen
    });

    console.log(`Response transaction => ${moment().format()} - ${result.process || ''} - ${req.method} - ${origen} - ${result.status} - ${result.success}`);
  }
}