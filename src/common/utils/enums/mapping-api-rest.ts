export enum MethodMessage {
    GETBYID = ':Id',
    GETALL = '/',
    UPDATE = ':Id',
    POST = '/',
}

export enum MappingApiRest {
    VERSION = '/V1',
    CONTROLLER= '/Shedule',
   
}
