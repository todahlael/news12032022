export enum TerminalesGeneral {
    "GPS" = "GPS",
    "NFS" = "NFC",
    "Gama" = "Gama",
    "BANDAS_DISPONIBLES" = "BANDAS_DISPONIBLES",
    "DESBLOQUEO" = "DESBLOQUEO",
    "FRECUENCIAS" = "FRECUENCIAS",
    "MEMORIA_INTERNA" = "MEMORIA_INTERNA",
    "MEMORIA_ROM" = "MEMORIA_ROM",
    "TIPO_DE_PANTALLA" = "TIPO_DE_PANTALLA",
    "WIFI" = "WIFI",
    "DISENO_DE_PANTALLA" = "DISENO_DE_PANTALLA",
    "FORMATO_EQUIPO" = "FORMATO_EQUIPO",
    "MEMORIA_RAM" = "MEMORIA_RAM",
    "TAMANO_DEL_EQUIPO" = "TAMANO_DEL_EQUIPO",
    "FabricanteEquipos" = "FabricanteEquipos",
    "TecnologiaEquipos" = "TecnologiaEquipos",
    "CLASIFICACION_FISCAL" = "CLASIFICACION_FISCAL",
    "GarantiaDiasFabricante" = "GarantiaDiasFabricante",
    "GarantiaDiasClaro" = "GarantiaDiasClaro",
    "category" = "category",
    "productTechnicalType" = "productTechnicalType",
    "Color" = "Color",
    "ModeloEquipos" = "ModeloEquipos",
    "specificationSubtype" = "specificationSubtype",
    "CLASIFICACION" = "CLASIFICACION"
}

export enum PosPlaDat {
    "TipoServicio" = "TipoServicio",
    "Gama" = "Gama",
    "AppsMovilComercial" = "AppsMovilComercial",
    "LdiMovilComercial" = "LdiMovilComercial",
    "DatosMovilComercial" = "DatosMovilComercial",
    "SmsMovilComercial" = "SmsMovilComercial",
    "VozMovilComercial" = "VozMovilComercial",
    "Kit" = "Kit",
    "custom13" = "custom13",
    "custom13Deg" = "custom13Deg",
    "market" = "market",
    "SegmentoTarifario" = "SegmentoTarifario",
    "NombreCorto" = "NombreCorto",
    "TipoPlan" = "TipoPlan"
}

export enum TecnologiaGeneral {
    "GarantiaDiasClaro" = "GarantiaDiasClaro",
    "GarantiaDiasFabricante" = "GarantiaDiasFabricante",
    "CLASIFICACION_FISCAL" = "CLASIFICACION_FISCAL",
    "TIPO DE PANTALLA" = "TIPO DE PANTALLA",
    "ALMACENAMIENTO TECNOLOGIA" = "ALMACENAMIENTO TECNOLOGIA",
    "ALMACENAMIENTO" = "ALMACENAMIENTO",
    "MEMORIA" = "MEMORIA",
    "CONTENIDO" = "CONTENIDO",
    "CONECTIVIDAD TECNOLOGIA" = "CONECTIVIDAD TECNOLOGIA",
    "CONECTIVIDAD" = "CONECTIVIDAD",
    "RESOLUCIOPANTALLAN" = "RESOLUCIOPANTALLAN",
    "PANTALLA_TEC" = "PANTALLA_TEC",
    "PROCESADOR_TEC" = "PROCESADOR_TEC",
    "SISTEMA_OPERATIVO" = "SISTEMA_OPERATIVO",
    "PROCESADORTECNOLOGIA" = "PROCESADORTECNOLOGIA",
    "RESOLUCION PANTALLA" = "RESOLUCION PANTALLA",
    "TIPO DE PRODUCTO" = "TIPO DE PRODUCTO",
    "RESOLUCION" = "RESOLUCION",
    "TAMANOPANTALLA" = "TAMANOPANTALLA",
    "TIPO_PANTALLA" = "TIPO_PANTALLA",
    "FabricanteTecnologia" = "FabricanteTecnologia",
    "productTechnicalType" = "productTechnicalType",
    "category" = "category",
    "COLOR TECNOLOGIA" = "COLOR TECNOLOGIA",
    "COLOR_TEC" = "COLOR_TEC",
    "specificationSubtype" = "specificationSubtype"
}

export enum PosPlaMov {
    "TipoServicio" = "TipoServicio",
    "Gama" = "Gama",
    "AppsMovilComercial" = "AppsMovilComercial",
    "LdiMovilComercial" = "LdiMovilComercial",
    "DatosMovilComercial" = "DatosMovilComercial",
    "SmsMovilComercial" = "SmsMovilComercial",
    "VozMovilComercial" = "VozMovilComercial",
    "Kit" = "Kit",
    "custom13" = "custom13",
    "custom13Deg" = "custom13Deg",
    "market" = "market",
    "SegmentoTarifario" = "SegmentoTarifario",
    "NombreCorto" = "NombreCorto",
    "TipoPlan" = "TipoPlan"
}

export enum PrePla {
    "TipoServicio" = "TipoServicio",
    "Gama" = "Gama",
    "AppsMovilComercial" = "AppsMovilComercial",
    "DatosMovilComercial" = "DatosMovilComercial",
    "SmsMovilComercial" = "SmsMovilComercial",
    "VozMovilComercial" = "VozMovilComercial",
    "Kit" = "Kit",
    "custom13" = "custom13",
    "custom13Deg" = "custom13Deg",
    "market" = "market",
    "SegmentoTarifario" = "SegmentoTarifario",
    "NombreCorto" = "NombreCorto",
    "TipoPlan" = "TipoPlan"
}