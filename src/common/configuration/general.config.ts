export default {
  apiMapping: process.env.API_MAPPING || '/RSAbsoluteCalendarAlarmFeaturesETL',
  apiVersion: process.env.API_VERSION || '/V1',
  controllerMockup: process.env.CONTROLLER || '',
  controllerCategories: process.env.CONTROLLER || '/Categories',
  port: process.env.PORT || 3001,
  logLevel: process.env.LOG_LEVEL || 'ALL',
  categories: process.env.CATEGORIES || 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs',
  catalogs: process.env.CATALOGS || 'http://100.126.21.189:7898/CatalogManagement/v1.0/productOffering?idBusinessTransaction=12345678&idApplication=1234&target=ECM&userApplication=userApplication&password=2343&startDate=2020-05-21T10:00:00&ipApplication=100.123.212.123&idESBTransaction=12344456&userSession=userSession&additionalNode=addNotes&channel=USSD&fields=id,name,description,family,category,speificationType,specificationSubtype,characteristics&productCategory='
};