import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import Logging from 'src/common/lib/logging';
import { Etask } from 'src/common/utils/enums/taks.enum';
import { IJobDTO } from 'src/controller/dto/jobcron/job.dto';
import { ResponseService } from 'src/controller/dto/response-job.dto';
import { IJob } from 'src/core/entity/job.entity';
import { IJobProvider } from '../job.provider';
import { CatalogTerminalesgModel } from '../model/catalogterminales.model';
import { JobModel } from '../model/job.model';
import { MessageModel } from '../model/message.model';


@Injectable()
export class JobProvider implements IJobProvider {

    private readonly logger = new Logging(JobProvider.name);

    constructor(
        @InjectModel(MessageModel.name) private readonly jobModel: Model<MessageModel>,
        @InjectModel(CatalogTerminalesgModel.name) private readonly catalogTerminalModel: Model<CatalogTerminalesgModel>
    ) { }


    async updateMessage(iJob: string): Promise<any> {

        try {
            let id = { _id: iJob };

            console.log("aqui va todo lo que necesitasssssssss",iJob)           

            let crontime =
            {
                status: "delete"
            }

             return await this.catalogTerminalModel.findByIdAndUpdate(id, crontime, {
                 new: true,
                 upsert: true,
             });

        } catch (error) {
            throw new ResponseService(
                true,
                "internal server error",
                500
            )
        }
    }

    async getHour(): Promise<any> {

        try {

            //let respuesta = await this.jobModel.findById(id);
            let respuesta = await this.catalogTerminalModel.find({ status: undefined })
            return respuesta

          

        } catch (error) {
            throw new ResponseService(
                true,
                "internal server error",
                500
            )

        }

    }
   

    async getHourCron() {
        let id =
            { _id: "61258fd4c8f0494e3cb82024" };
        let respuesta = await this.jobModel.findById(id);
        let convertirHora = respuesta.hour;
        //console.log("HORA COLOMBIA", convertirHora)
        this.logger.write(convertirHora, Etask.HS_COL, false);
        let convertirHoraArr: any = convertirHora.split(' ');
        convertirHoraArr = `${convertirHoraArr[0]} ${convertirHoraArr[1] * 1 + 5 * 1} * * *`;
        //console.log("HORA UTC", convertirHoraArr)
        this.logger.write(convertirHoraArr, Etask.HS_UTC, false);
        return convertirHoraArr;

    }
}