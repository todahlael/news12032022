let historias = {
    "hits": [
        {
            "created_at": "2022-03-12T16:53:05.000Z",
            "title": null,
            "url": null,
            "author": "bazzargh",
            "points": null,
            "story_text": null,
            "comment_text": "I had this problem too, so I wrote some code. It&#x27;s not open sourced yet and who knows if I&#x27;ll get round to it, but here&#x27;s a detailed description of how it works, it is <i>not a lot of code</i> so you could probably reproduce this easily..<p>The problem I wanted to solve is more whiteboarding and presentation: I often am in calls describing something that only needs fairly simple drawings - boxes, lines, labels - but whiteboard where you scribble or even lay out rectangles makes me sweat the small stuff, resizing and moving everything all the time when the diagram changes. Also, I wanted animation: to show transition between states. I&#x27;d tried using mingrammer to do diagrams as code, but I find graphviz layouts unpredictable. Most often what I want is: just boxes, lines, labels, size things to fit, keep everything &#x27;evenly spaced&#x27;.<p>So, I wrote some JS (it&#x27;s ~700 lines of code, no external libraries). I divide a web page into two areas: one textarea where I type commands line-by-line, and the drawing, which is svg. On selectionchange events for the textarea, I debounce for 500ms, so if I&#x27;ve paused the layout&amp;animation kick in. The drawing state has a map of nodes (boxes), which contain a title, an icon, map of connections to other boxes, and a list of children (ids of other nodes), as well as recording the node we are currently zoomed in to, the node we are currently editing, and a few other details. There is a node called &#x27;top&#x27; which is the starting point.<p>The commands are pretty simple and were added as I needed things, eg:<p><pre><code>    add internet # add a child to the currently edited node with id &#x27;internet&#x27;\n                 # its title and icon are also set to &#x27;internet&#x27;\n    edit internet\n    add host1 icon host # this has id host1, title host1, icon host\n    add host2 icon host title &quot;banana stand&quot; # other attrs are layout and stroke\n    edit top\n    edit aws # editing a non-existent node adds it\n    add ec2\n    arrow host1 ec2\n    zoom aws # this node expands to fill the screen\n    top # same as &#x27;zoom top&#x27;, zooms to the top level\n</code></pre>\nIt&#x27;s interpreted line-by-line so that each line represents one state of the diagram. There are commands to delete nodes: when I delete a node I just remove it from its parent but leave it in the top-level state. That has the neat effect that if I re-add it, I get the node with all its descendents and connections restored in one step, which I can use to pre-diagram things I talk about often.<p>After calculating the drawing state by applying all the commands from the start to the current selection, the next step is to limit this to the visible pieces. I make a copy of the drawing state, starting from the currently zoomed node and following all children. Then I add all connections, if all the &#x27;to&#x27; ends of the connections are visible.<p>Next, I do layout. Starting with the visible tree, annotate all nodes with positions of the box (if any), the icon, and the label. The diagrams I&#x27;m drawing are similar to those produced by AWS Perspective: <a href=\"https:&#x2F;&#x2F;aws.amazon.com&#x2F;solutions&#x2F;implementations&#x2F;aws-perspective&#x2F;\" rel=\"nofollow\">https:&#x2F;&#x2F;aws.amazon.com&#x2F;solutions&#x2F;implementations&#x2F;aws-perspec...</a> , so if a node has no children I draw it as a large icon with a label below, if it has children, it is a box with a small icon to the top left, a centred label at the top. Each node can choose one of a small number of layouts that I can do automatically with just a list of children: &#x27;ring&#x27; (a circle of nodes), &#x27;row&#x27;, &#x27;column&#x27;, or &#x27;snake&#x27; (the default: alternate rtl-ltr rows to evenly fill the space; this will be a grid if that fits or could end up like 4-3-4-3 if it doesn&#x27;t). In ring &amp; snake, boxes are always 4:3; in row and column they are stretched to fit.<p>Next, I do animation. I keep around the previous layed-out state, and use window.requestAnimationFrame to calculate the position of boxes between the start and end state. A box that is in both start and end states is moved, if it is only in start or end I fade it in or out as need be. This lets me animate between _any_ two states of the drawing, so I can talk about one bit of the diagram, then jump back and forth by clicking in the command window, and it smoothly animates between them. I found animating for just 0.5s worked best for interactivity; it&#x27;s nice to see a slower move but it feels laggy when typing.<p>I calculate arrow positions after calculating the final position of boxes and icons. I chose to use circular arcs, because you will never get an awkward situation where an arc lies directly along the edge of a box; straight things are always boxes, curvy things are always arrows. SVG wants two endpoints and a centre to draw these. So, I start with an arc between the centres of the two boxes, choose a radius twice as long as the distance between these points; then I calculate the intersection of the arc with the boxes, and use those two intersection points as the start&#x2F;end of the arc. (this isn&#x27;t that difficult, the formula for the arc is in the svg spec, and it&#x27;s checking 4 straight lines, choose the intersection point closest to the other box). Like the boxes, the arrows fade in and out if they are not needed in one of the start or end states.<p>All of this then just replaces the content of the svg. It&#x27;s surprisingly smooth.<p>One last detail is icons. I&#x27;m using the icons from mingrammer (<a href=\"https:&#x2F;&#x2F;github.com&#x2F;mingrammer&#x2F;diagrams&#x2F;tree&#x2F;master&#x2F;resources\" rel=\"nofollow\">https:&#x2F;&#x2F;github.com&#x2F;mingrammer&#x2F;diagrams&#x2F;tree&#x2F;master&#x2F;resources</a>), which gives me about 1600(!). Finding an icon _while you type_ is awkward and initially I had to drop to the shell to find the file I was going to refer to. I tried giving the drawing tool a mode that would let me visually pick the icon, but 1600 is too many. So I changed it to use a fuzzy search to find an appropriate icon: it looks for the icon where the sequence of characters appear in the shortest substring of the icon path: eg for &#x27;ec2&#x27; it constructs the regex `.*(e.*?c.*?2)`, scoring the matching substring &#x27;ec2&#x27; better than &#x27;elastic2&#x27;, and the shorter containing string &#x27;aws&#x2F;compute&#x2F;ec2&#x27; better than eg &#x27;aws&#x2F;compute&#x2F;ec2-rounded&#x27;. (I have a further round of preferences so that the top level aws iconset is preferred to eg the ibm one, which has terrible icons). This gives you an icon for almost anything you type, and encourages a more playful approach to picking the icon than the exact-match approach.<p>There&#x27;s a bit more to it, I also accept some markdown which fades from the diagram to slides with bullet points, then back to the diagram if the current command is a diagramming command. But the description above is most of it. I could probably have done this better with eg d3 to do the drawing but I am not a front end developer at all and the whole thing was more of a hack over a couple of weekends. I should clean it up a bit, but it works.<p>I serve up pre-prepared pages with this js attached from github pages, I can walk through eg the flow of data clicking the down arrow to change the selection which causes it to animate to the next state which has the next arrow... and so on.",
            "num_comments": null,
            "story_id": 30631993,
            "story_title": "Ask HN: How to quickly animate system sketches and 2D diagrams?",
            "story_url": null,
            "parent_id": 30631993,
            "created_at_i": 1647103985,
            "_tags": [
                "comment",
                "author_bazzargh",
                "story_30631993"
            ],
            "objectID": "30652684",
            "_highlightResult": {
                "author": {
                    "value": "bazzargh",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "I had this problem too, so I wrote some code. It's not open sourced yet and who knows if I'll get round to it, but here's a detailed description of how it works, it is <i>not a lot of code</i> so you could probably reproduce this easily..<p>The problem I wanted to solve is more whiteboarding and presentation: I often am in calls describing something that only needs fairly simple drawings - boxes, lines, labels - but whiteboard where you scribble or even lay out rectangles makes me sweat the small stuff, resizing and moving everything all the time when the diagram changes. Also, I wanted animation: to show transition between states. I'd tried using mingrammer to do diagrams as code, but I find graphviz layouts unpredictable. Most often what I want is: just boxes, lines, labels, size things to fit, keep everything 'evenly spaced'.<p>So, I wrote some JS (it's ~700 lines of code, no external libraries). I divide a web page into two areas: one textarea where I type commands line-by-line, and the drawing, which is svg. On selectionchange events for the textarea, I debounce for 500ms, so if I've paused the layout&amp;animation kick in. The drawing state has a map of <em>nodes</em> (boxes), which contain a title, an icon, map of connections to other boxes, and a list of children (ids of other <em>nodes</em>), as well as recording the node we are currently zoomed in to, the node we are currently editing, and a few other details. There is a node called 'top' which is the starting point.<p>The commands are pretty simple and were added as I needed things, eg:<p><pre><code>    add internet # add a child to the currently edited node with id 'internet'\n                 # its title and icon are also set to 'internet'\n    edit internet\n    add host1 icon host # this has id host1, title host1, icon host\n    add host2 icon host title &quot;banana stand&quot; # other attrs are layout and stroke\n    edit top\n    edit aws # editing a non-existent node adds it\n    add ec2\n    arrow host1 ec2\n    zoom aws # this node expands to fill the screen\n    top # same as 'zoom top', zooms to the top level\n</code></pre>\nIt's interpreted line-by-line so that each line represents one state of the diagram. There are commands to delete <em>nodes</em>: when I delete a node I just remove it from its parent but leave it in the top-level state. That has the neat effect that if I re-add it, I get the node with all its descendents and connections restored in one step, which I can use to pre-diagram things I talk about often.<p>After calculating the drawing state by applying all the commands from the start to the current selection, the next step is to limit this to the visible pieces. I make a copy of the drawing state, starting from the currently zoomed node and following all children. Then I add all connections, if all the 'to' ends of the connections are visible.<p>Next, I do layout. Starting with the visible tree, annotate all <em>nodes</em> with positions of the box (if any), the icon, and the label. The diagrams I'm drawing are similar to those produced by AWS Perspective: <a href=\"https://aws.amazon.com/solutions/implementations/aws-perspective/\" rel=\"nofollow\">https://aws.amazon.com/solutions/implementations/aws-perspec...</a> , so if a node has no children I draw it as a large icon with a label below, if it has children, it is a box with a small icon to the top left, a centred label at the top. Each node can choose one of a small number of layouts that I can do automatically with just a list of children: 'ring' (a circle of <em>nodes</em>), 'row', 'column', or 'snake' (the default: alternate rtl-ltr rows to evenly fill the space; this will be a grid if that fits or could end up like 4-3-4-3 if it doesn't). In ring &amp; snake, boxes are always 4:3; in row and column they are stretched to fit.<p>Next, I do animation. I keep around the previous layed-out state, and use window.requestAnimationFrame to calculate the position of boxes between the start and end state. A box that is in both start and end states is moved, if it is only in start or end I fade it in or out as need be. This lets me animate between _any_ two states of the drawing, so I can talk about one bit of the diagram, then jump back and forth by clicking in the command window, and it smoothly animates between them. I found animating for just 0.5s worked best for interactivity; it's nice to see a slower move but it feels laggy when typing.<p>I calculate arrow positions after calculating the final position of boxes and icons. I chose to use circular arcs, because you will never get an awkward situation where an arc lies directly along the edge of a box; straight things are always boxes, curvy things are always arrows. SVG wants two endpoints and a centre to draw these. So, I start with an arc between the centres of the two boxes, choose a radius twice as long as the distance between these points; then I calculate the intersection of the arc with the boxes, and use those two intersection points as the start/end of the arc. (this isn't that difficult, the formula for the arc is in the svg spec, and it's checking 4 straight lines, choose the intersection point closest to the other box). Like the boxes, the arrows fade in and out if they are not needed in one of the start or end states.<p>All of this then just replaces the content of the svg. It's surprisingly smooth.<p>One last detail is icons. I'm using the icons from mingrammer (<a href=\"https://github.com/mingrammer/diagrams/tree/master/resources\" rel=\"nofollow\">https://github.com/mingrammer/diagrams/tree/master/resources</a>), which gives me about 1600(!). Finding an icon _while you type_ is awkward and initially I had to drop to the shell to find the file I was going to refer to. I tried giving the drawing tool a mode that would let me visually pick the icon, but 1600 is too many. So I changed it to use a fuzzy search to find an appropriate icon: it looks for the icon where the sequence of characters appear in the shortest substring of the icon path: eg for 'ec2' it constructs the regex `.*(e.*?c.*?2)`, scoring the matching substring 'ec2' better than 'elastic2', and the shorter containing string 'aws/compute/ec2' better than eg 'aws/compute/ec2-rounded'. (I have a further round of preferences so that the top level aws iconset is preferred to eg the ibm one, which has terrible icons). This gives you an icon for almost anything you type, and encourages a more playful approach to picking the icon than the exact-match approach.<p>There's a bit more to it, I also accept some markdown which fades from the diagram to slides with bullet points, then back to the diagram if the current command is a diagramming command. But the description above is most of it. I could probably have done this better with eg d3 to do the drawing but I am not a front end developer at all and the whole thing was more of a hack over a couple of weekends. I should clean it up a bit, but it works.<p>I serve up pre-prepared pages with this js attached from github pages, I can walk through eg the flow of data clicking the down arrow to change the selection which causes it to animate to the next state which has the next arrow... and so on.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Ask HN: How to quickly animate system sketches and 2D diagrams?",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-12T12:56:12.000Z",
            "title": "Consume Web and Node.js streams using async/await",
            "url": "https://www.nodejsdesignpatterns.com/blog/node-js-stream-consumer/",
            "author": "loige",
            "points": 3,
            "story_text": null,
            "comment_text": null,
            "num_comments": 0,
            "story_id": null,
            "story_title": null,
            "story_url": null,
            "parent_id": null,
            "created_at_i": 1647089772,
            "_tags": [
                "story",
                "author_loige",
                "story_30651037"
            ],
            "objectID": "30651037",
            "_highlightResult": {
                "title": {
                    "value": "Consume Web and Node.js streams using async/await",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "url": {
                    "value": "https://www.<em>nodejs</em>designpatterns.com/blog/node-js-stream-consumer/",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "author": {
                    "value": "loige",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-11T12:47:05.000Z",
            "title": "Read a Node.js stream using async/await",
            "url": "https://www.nodejsdesignpatterns.com/blog/node-js-stream-consumer/",
            "author": "loige",
            "points": 1,
            "story_text": null,
            "comment_text": null,
            "num_comments": 0,
            "story_id": null,
            "story_title": null,
            "story_url": null,
            "parent_id": null,
            "created_at_i": 1647002825,
            "_tags": [
                "story",
                "author_loige",
                "story_30639137"
            ],
            "objectID": "30639137",
            "_highlightResult": {
                "title": {
                    "value": "Read a Node.js stream using async/await",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "url": {
                    "value": "https://www.<em>nodejs</em>designpatterns.com/blog/node-js-stream-consumer/",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "author": {
                    "value": "loige",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-10T22:24:27.000Z",
            "title": "Node.js – v17.7.1",
            "url": "https://nodejs.org/en/blog/release/v17.7.1/",
            "author": "bricss",
            "points": 1,
            "story_text": null,
            "comment_text": null,
            "num_comments": 0,
            "story_id": null,
            "story_title": null,
            "story_url": null,
            "parent_id": null,
            "created_at_i": 1646951067,
            "_tags": [
                "story",
                "author_bricss",
                "story_30633377"
            ],
            "objectID": "30633377",
            "_highlightResult": {
                "title": {
                    "value": "Node.js – v17.7.1",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "url": {
                    "value": "https://<em>nodejs</em>.org/en/blog/release/v17.7.1/",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "author": {
                    "value": "bricss",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-10T16:29:38.000Z",
            "title": null,
            "url": null,
            "author": "claytongulick",
            "points": null,
            "story_text": null,
            "comment_text": "&lt;raises hand&gt;<p>I&#x27;ve worked extensively with both.<p>I find that ts normally brings a downside, actually. In one project I maintain, a solid 30% of the code is nothing but ts stuff.<p>It makes maintaining the code base excruciating.<p>Small changes between versions of ts or .d.ts files have caused hundreds of errors. A concrete example of this is the change in mongo driver that forced &quot;new ObjectId()&quot; instead of &quot;ObjectId()&quot; though functionally there was no difference.<p>I see massive bizarre and difficult to grok type declarations that grow in excruciatingly byzantine complexity just to satisfy the compiler for non-trivial flexible function definitions.<p>I see painful compilation times, a beefy laptop&#x27;s CPU getting pegged and the fan whining while trying to do even minor updates.<p>Try to do a build while on a video conference? I see two to three minute build times.<p>I see a series of nonsense stories in sprints that are nothing more than activity over achievement. They don&#x27;t drive product functionality, they are a bunch of make-work to satisfy the developer asthetic and the compiler.<p>The nightmare of &quot;compiled&quot; NodeJS production support is a dystopian hellscape that I&#x27;m forced to endure every day because of NestJS and typescript. And nothing in that has done <i>anything</i> to increase code quality or reduce bugs from the crappy offshore team that wrote it.<p>Clearly, I&#x27;m not a fan.<p>That being said, I think type comments in jsdoc are a honking good idea.<p>I use inline &#x2F;* @type *&#x2F; declarations and .d.ts files liberally.<p>If we could add to jsdoc the ability to document types and purpose of arbitrary js objects inline, at definition time, instead of an external .d.ts file, I&#x27;d be a happy guy.<p>An example of this would be sequelize schema defs, or mongoose schema defs, where I can have all my jsdoc code comments and type definitions in the same place as the code.<p>All that being said, I like the idea of this proposal. Especially if it could solve the schema definition issue above.<p>I think optional typing is a good idea to bake directly into the language. I thought AS3 did a great job of this.<p>I even think it would be great to have an equivalent of &quot;strict&quot; that would enforce typing at the module level to enable straightforward AOT &#x2F; wasm interop (I know it doesn&#x27;t solve the GC issue).<p>My beef is mostly with the extra compilation step and needless complexity of ts.<p>If we had simple (no generics) opt-in typing that was natively supported by the engine, I think it would enhance the language overall, like type hints have done for python.",
            "num_comments": null,
            "story_id": 30626458,
            "story_title": "First look: adding type annotations to JavaScript",
            "story_url": "https://2ality.com/2022/03/type-annotations-first-look.html",
            "parent_id": 30628033,
            "created_at_i": 1646929778,
            "_tags": [
                "comment",
                "author_claytongulick",
                "story_30626458"
            ],
            "objectID": "30628746",
            "_highlightResult": {
                "author": {
                    "value": "claytongulick",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "&lt;raises hand&gt;<p>I've worked extensively with both.<p>I find that ts normally brings a downside, actually. In one project I maintain, a solid 30% of the code is nothing but ts stuff.<p>It makes maintaining the code base excruciating.<p>Small changes between versions of ts or .d.ts files have caused hundreds of errors. A concrete example of this is the change in mongo driver that forced &quot;new ObjectId()&quot; instead of &quot;ObjectId()&quot; though functionally there was no difference.<p>I see massive bizarre and difficult to grok type declarations that grow in excruciatingly byzantine complexity just to satisfy the compiler for non-trivial flexible function definitions.<p>I see painful compilation times, a beefy laptop's CPU getting pegged and the fan whining while trying to do even minor updates.<p>Try to do a build while on a video conference? I see two to three minute build times.<p>I see a series of nonsense stories in sprints that are nothing more than activity over achievement. They don't drive product functionality, they are a bunch of make-work to satisfy the developer asthetic and the compiler.<p>The nightmare of &quot;compiled&quot; <em>NodeJS</em> production support is a dystopian hellscape that I'm forced to endure every day because of NestJS and typescript. And nothing in that has done <i>anything</i> to increase code quality or reduce bugs from the crappy offshore team that wrote it.<p>Clearly, I'm not a fan.<p>That being said, I think type comments in jsdoc are a honking good idea.<p>I use inline /* @type */ declarations and .d.ts files liberally.<p>If we could add to jsdoc the ability to document types and purpose of arbitrary js objects inline, at definition time, instead of an external .d.ts file, I'd be a happy guy.<p>An example of this would be sequelize schema defs, or mongoose schema defs, where I can have all my jsdoc code comments and type definitions in the same place as the code.<p>All that being said, I like the idea of this proposal. Especially if it could solve the schema definition issue above.<p>I think optional typing is a good idea to bake directly into the language. I thought AS3 did a great job of this.<p>I even think it would be great to have an equivalent of &quot;strict&quot; that would enforce typing at the module level to enable straightforward AOT / wasm interop (I know it doesn't solve the GC issue).<p>My beef is mostly with the extra compilation step and needless complexity of ts.<p>If we had simple (no generics) opt-in typing that was natively supported by the engine, I think it would enhance the language overall, like type hints have done for python.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "First look: adding type annotations to JavaScript",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://2ality.com/2022/03/type-annotations-first-look.html",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-10T15:55:14.000Z",
            "title": null,
            "url": null,
            "author": "spion",
            "points": null,
            "story_text": null,
            "comment_text": "Of course, using a bundler means you&#x27;ll face several other unpleasant realities. Like<p><pre><code>  - what about tests, how do they run\n  - source maps support in node being 3rd party and unbearably slow for any medium sized project \n    - edit: this has been fixed somewhat recently https:&#x2F;&#x2F;nodejs.medium.com&#x2F;source-maps-in-node-js-482872b56116 \n    - only the 3rd party bit, not the slowness bit https:&#x2F;&#x2F;github.com&#x2F;nodejs&#x2F;node&#x2F;issues&#x2F;41541\n    - also applicable when not bundling, albiet fewer reprcutions to turning off source maps\n  - poor native (C++) modules support in bundlers \n  - modules with really dynamic `require` statements being supported at varying levels by varying bundlers</code></pre>",
            "num_comments": null,
            "story_id": 30626458,
            "story_title": "First look: adding type annotations to JavaScript",
            "story_url": "https://2ality.com/2022/03/type-annotations-first-look.html",
            "parent_id": 30627458,
            "created_at_i": 1646927714,
            "_tags": [
                "comment",
                "author_spion",
                "story_30626458"
            ],
            "objectID": "30628312",
            "_highlightResult": {
                "author": {
                    "value": "spion",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "Of course, using a bundler means you'll face several other unpleasant realities. Like<p><pre><code>  - what about tests, how do they run\n  - source maps support in node being 3rd party and unbearably slow for any medium sized project \n    - edit: this has been fixed somewhat recently https://<em>nodejs</em>.medium.com/source-maps-in-node-js-482872b56116 \n    - only the 3rd party bit, not the slowness bit https://github.com/<em>nodejs</em>/node/issues/41541\n    - also applicable when not bundling, albiet fewer reprcutions to turning off source maps\n  - poor native (C++) modules support in bundlers \n  - modules with really dynamic `require` statements being supported at varying levels by varying bundlers</code></pre>",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "First look: adding type annotations to JavaScript",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://2ality.com/2022/03/type-annotations-first-look.html",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T22:29:53.000Z",
            "title": null,
            "url": null,
            "author": "coreyp_1",
            "points": null,
            "story_text": null,
            "comment_text": "I wrote a program to distribute my lecture slides to my students in real time.<p>When I was a University Professor, I taught Theory of Computing (among other classes).  I did not want to maintain two sets of slides (fill-in-the-blank style), and I didn&#x27;t want the students to have to write non-stop during the lecture, so I wrote a couple of programs to distribute the slides in real time during the lecture.<p>Part 1: C++.  A program ran on my computer.  When I pressed a button, it took a screen shot of my chosen monitor and sent it to my server.  Communication via websockets.<p>Part 2: Browser. Students connected to my server at the start of class.  As I lectured, the slides appeared in their browser automatically.  Of course, they could click through all previous slides (for that day as well as previous lectures), but if they were not actively browsing the slides, then the newest slide presented itself to them automatically.  (Technically, it was always one slide behind, because I would not release a slide until I was done talking about it.)  Communication via websockets. It worked in lectures attended by 100+ students.<p>Part 3: NodeJS. My server received screenshots from me (as well as some metadata), kept a small database of all past lectures&#x2F;screenshots, and served a browsable interface to the students.  It routed all the websocket connections so that everything &quot;just worked&quot;TM.<p>I thought it was cool.  The students complained that the slides were not searchable like a PDF.  I directed them to the index of the textbook.",
            "num_comments": null,
            "story_id": 30614623,
            "story_title": "Some tiny personal programs I've written",
            "story_url": "https://jvns.ca/blog/2022/03/08/tiny-programs/",
            "parent_id": 30614623,
            "created_at_i": 1646864993,
            "_tags": [
                "comment",
                "author_coreyp_1",
                "story_30614623"
            ],
            "objectID": "30620835",
            "_highlightResult": {
                "author": {
                    "value": "coreyp_1",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "I wrote a program to distribute my lecture slides to my students in real time.<p>When I was a University Professor, I taught Theory of Computing (among other classes).  I did not want to maintain two sets of slides (fill-in-the-blank style), and I didn't want the students to have to write non-stop during the lecture, so I wrote a couple of programs to distribute the slides in real time during the lecture.<p>Part 1: C++.  A program ran on my computer.  When I pressed a button, it took a screen shot of my chosen monitor and sent it to my server.  Communication via websockets.<p>Part 2: Browser. Students connected to my server at the start of class.  As I lectured, the slides appeared in their browser automatically.  Of course, they could click through all previous slides (for that day as well as previous lectures), but if they were not actively browsing the slides, then the newest slide presented itself to them automatically.  (Technically, it was always one slide behind, because I would not release a slide until I was done talking about it.)  Communication via websockets. It worked in lectures attended by 100+ students.<p>Part 3: <em>NodeJS</em>. My server received screenshots from me (as well as some metadata), kept a small database of all past lectures/screenshots, and served a browsable interface to the students.  It routed all the websocket connections so that everything &quot;just worked&quot;TM.<p>I thought it was cool.  The students complained that the slides were not searchable like a PDF.  I directed them to the index of the textbook.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Some tiny personal programs I've written",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://jvns.ca/blog/2022/03/08/tiny-programs/",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T21:34:50.000Z",
            "title": null,
            "url": null,
            "author": "armchairhacker",
            "points": null,
            "story_text": null,
            "comment_text": "why not just make JavaScript engines like node and browsers strip out TypeScript? Hide it under a flag if you want, it’s developers who are debugging who would benefit from that the most.<p>I mean this is a great idea, removing one more of the many many complexities in simply getting a nodejs package to browser and debugging (getting accurate source info and avoiding transpile errors are a PITA). JavaScript is an interpreted language yet 90% of the time it’s not even run interpreted. But “a proposal for Type Syntax in JavaScript” when TypeScript <i>and</i> JSDoc exist is just confusing and redundant.",
            "num_comments": null,
            "story_id": 30618681,
            "story_title": "A Proposal for Type Syntax in JavaScript",
            "story_url": "https://devblogs.microsoft.com/typescript/a-proposal-for-type-syntax-in-javascript/",
            "parent_id": 30618681,
            "created_at_i": 1646861690,
            "_tags": [
                "comment",
                "author_armchairhacker",
                "story_30618681"
            ],
            "objectID": "30620306",
            "_highlightResult": {
                "author": {
                    "value": "armchairhacker",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "why not just make JavaScript engines like node and browsers strip out TypeScript? Hide it under a flag if you want, it’s developers who are debugging who would benefit from that the most.<p>I mean this is a great idea, removing one more of the many many complexities in simply getting a <em>nodejs</em> package to browser and debugging (getting accurate source info and avoiding transpile errors are a PITA). JavaScript is an interpreted language yet 90% of the time it’s not even run interpreted. But “a proposal for Type Syntax in JavaScript” when TypeScript <i>and</i> JSDoc exist is just confusing and redundant.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "A Proposal for Type Syntax in JavaScript",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://devblogs.microsoft.com/typescript/a-proposal-for-type-syntax-in-javascript/",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T20:27:43.000Z",
            "title": null,
            "url": null,
            "author": "danicoy",
            "points": null,
            "story_text": null,
            "comment_text": "<p><pre><code>  Location: Guayaquil, Ecuador\n  Remote: Yes\n  Willing to relocate: Yes\n  Technologies: Flutter, Dart, Javascript, HTML, CSS, NodeJS, Firebase, MongoDB, Parse Server\n  Résumé&#x2F;CV: https:&#x2F;&#x2F;dctech.dev&#x2F;downloads&#x2F;CV%20Daniel%20Coyula.pdf\n  Email: danicoy@gmail.com\n  Web: https:&#x2F;&#x2F;dctech.dev</code></pre>",
            "num_comments": null,
            "story_id": 30515748,
            "story_title": "Ask HN: Who wants to be hired? (March 2022)",
            "story_url": null,
            "parent_id": 30515748,
            "created_at_i": 1646857663,
            "_tags": [
                "comment",
                "author_danicoy",
                "story_30515748"
            ],
            "objectID": "30619547",
            "_highlightResult": {
                "author": {
                    "value": "danicoy",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "<p><pre><code>  Location: Guayaquil, Ecuador\n  Remote: Yes\n  Willing to relocate: Yes\n  Technologies: Flutter, Dart, Javascript, HTML, CSS, <em>NodeJS</em>, Firebase, MongoDB, Parse Server\n  Résumé/CV: https://dctech.dev/downloads/CV%20Daniel%20Coyula.pdf\n  Email: danicoy@gmail.com\n  Web: https://dctech.dev</code></pre>",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Ask HN: Who wants to be hired? (March 2022)",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T18:42:47.000Z",
            "title": null,
            "url": null,
            "author": "fyrepuffs",
            "points": null,
            "story_text": null,
            "comment_text": "The past couple of years have seen a push from cloud-centric companies telling the embedded Linux ecosystem and community that we should make do with cloud technologies like Golang, NodeJS, and Docker. However, most are unaware of the challenges of cramming cloud tools onto resource-constrained embedded systems. Even though some of these frameworks have the right intention, for example, LXD containers, most lack the initiative to understand the specific requirements of embedded Linux devices.",
            "num_comments": null,
            "story_id": 30618394,
            "story_title": "Why Embedded Linux Needs a Container Manager Written in C",
            "story_url": "https://pantacor.com/blog/embedded-linux-need-container-manager/",
            "parent_id": 30618394,
            "created_at_i": 1646851367,
            "_tags": [
                "comment",
                "author_fyrepuffs",
                "story_30618394"
            ],
            "objectID": "30618395",
            "_highlightResult": {
                "author": {
                    "value": "fyrepuffs",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "The past couple of years have seen a push from cloud-centric companies telling the embedded Linux ecosystem and community that we should make do with cloud technologies like Golang, <em>NodeJS</em>, and Docker. However, most are unaware of the challenges of cramming cloud tools onto resource-constrained embedded systems. Even though some of these frameworks have the right intention, for example, LXD containers, most lack the initiative to understand the specific requirements of embedded Linux devices.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Why Embedded Linux Needs a Container Manager Written in C",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://pantacor.com/blog/embedded-linux-need-container-manager/",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T18:23:52.000Z",
            "title": null,
            "url": null,
            "author": "ipnon",
            "points": null,
            "story_text": null,
            "comment_text": "I struggled so much as a new grad I wanted to give you some specific advice: There are two kinds of employee candidates, red flaggers and green flaggers. By this I mean people look at your resume and either think &quot;wow, what&#x27;s the catch?&quot; or &quot;hmm okay, maybe there&#x27;s something good here.&quot;<p>A red flagger would be someone who built a supercomputer out of beach pebbles then proved P=NP on it. The interviewer already knows you&#x27;re good enough for the job, they&#x27;re just looking for red flags that would disqualify you. A green flagger would be someone who worked at GenericCorp for 2 years on a Java queue. The interviewer knows you&#x27;re a programmer, but they need a signal that you&#x27;ll actually be able to do the job.<p>Why does this matter? If you&#x27;re a red flagger, all you need to do in the interview is not make an obvious mistake. They already want to hire you, they just have to do the HR dance first. If you&#x27;re a green flagger, you have to do everything right in the interview to get the job. They need programmers ASAP, but you still need to prove yourself. Being a green flagger is much harder than a red flagger. Becoming a red flagger is simple: Your interviewer needs to think you are smarter than them. If you pull this off they will always give you the benefit of the doubt. Your job interviewing will be so much easier.<p>To do this you just need one thing on your resume that they don&#x27;t know how to do. If your resume has an internship writing React, and a side project using NodeJS, your relative inexperience will make them think &quot;I can code circles around this chump.&quot; Switch that side project to a niche area they&#x27;ve never dabbled in, and suddenly they&#x27;re over their head. &quot;What do they know that I don&#x27;t? We must hire immediately!&quot;<p>I accomplished this by doing side projects in machine learning and crypto. For any programmer with some tenacity these are totally doable, but most people I interviewed with hadn&#x27;t done it themselves so I became a red flagger. It doesn&#x27;t need to be these subjects, it should be whatever niche computer topic you&#x27;re most interested in. It doesn&#x27;t need to be complicated either. If you can demo it, and it works, and you can explain it in a way that teaches the interviewer something, you&#x27;ve made the job of interviewing much easier.",
            "num_comments": null,
            "story_id": 30592572,
            "story_title": "Lessons from a tech job search",
            "story_url": "https://blog.nindalf.com/posts/tech-interview/",
            "parent_id": 30592834,
            "created_at_i": 1646850232,
            "_tags": [
                "comment",
                "author_ipnon",
                "story_30592572"
            ],
            "objectID": "30618134",
            "_highlightResult": {
                "author": {
                    "value": "ipnon",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "I struggled so much as a new grad I wanted to give you some specific advice: There are two kinds of employee candidates, red flaggers and green flaggers. By this I mean people look at your resume and either think &quot;wow, what's the catch?&quot; or &quot;hmm okay, maybe there's something good here.&quot;<p>A red flagger would be someone who built a supercomputer out of beach pebbles then proved P=NP on it. The interviewer already knows you're good enough for the job, they're just looking for red flags that would disqualify you. A green flagger would be someone who worked at GenericCorp for 2 years on a Java queue. The interviewer knows you're a programmer, but they need a signal that you'll actually be able to do the job.<p>Why does this matter? If you're a red flagger, all you need to do in the interview is not make an obvious mistake. They already want to hire you, they just have to do the HR dance first. If you're a green flagger, you have to do everything right in the interview to get the job. They need programmers ASAP, but you still need to prove yourself. Being a green flagger is much harder than a red flagger. Becoming a red flagger is simple: Your interviewer needs to think you are smarter than them. If you pull this off they will always give you the benefit of the doubt. Your job interviewing will be so much easier.<p>To do this you just need one thing on your resume that they don't know how to do. If your resume has an internship writing React, and a side project using <em>NodeJS</em>, your relative inexperience will make them think &quot;I can code circles around this chump.&quot; Switch that side project to a niche area they've never dabbled in, and suddenly they're over their head. &quot;What do they know that I don't? We must hire immediately!&quot;<p>I accomplished this by doing side projects in machine learning and crypto. For any programmer with some tenacity these are totally doable, but most people I interviewed with hadn't done it themselves so I became a red flagger. It doesn't need to be these subjects, it should be whatever niche computer topic you're most interested in. It doesn't need to be complicated either. If you can demo it, and it works, and you can explain it in a way that teaches the interviewer something, you've made the job of interviewing much easier.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Lessons from a tech job search",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://blog.nindalf.com/posts/tech-interview/",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T18:20:07.000Z",
            "title": "Node.js – v17.7.0",
            "url": "https://nodejs.org/en/blog/release/v17.7.0/",
            "author": "bricss",
            "points": 1,
            "story_text": null,
            "comment_text": null,
            "num_comments": 0,
            "story_id": null,
            "story_title": null,
            "story_url": null,
            "parent_id": null,
            "created_at_i": 1646850007,
            "_tags": [
                "story",
                "author_bricss",
                "story_30618087"
            ],
            "objectID": "30618087",
            "_highlightResult": {
                "title": {
                    "value": "Node.js – v17.7.0",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "url": {
                    "value": "https://<em>nodejs</em>.org/en/blog/release/v17.7.0/",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "author": {
                    "value": "bricss",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T15:41:57.000Z",
            "title": null,
            "url": null,
            "author": "mnkmnk",
            "points": null,
            "story_text": null,
            "comment_text": "I don’t think the average users ever delete any of their browser data. In this case however, the whole idea would be that there is redundancy and it is ok for you to clear your cache and storage. As for the hoster, they would probably use a nodejs type server to host their files instead of relying on having a browser tab open.<p>However, I do agree that relying on regular web browsers is not a good idea. A browser update could limit resources of background tabs even more than they normally do and soon your application is down.",
            "num_comments": null,
            "story_id": 30605252,
            "story_title": "Static torrent website with peer-to-peer queries over BitTorrent on 2M records",
            "story_url": "https://boredcaveman.xyz/post/0x2_static-torrent-website-p2p-queries.html",
            "parent_id": 30611359,
            "created_at_i": 1646840517,
            "_tags": [
                "comment",
                "author_mnkmnk",
                "story_30605252"
            ],
            "objectID": "30615659",
            "_highlightResult": {
                "author": {
                    "value": "mnkmnk",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "I don’t think the average users ever delete any of their browser data. In this case however, the whole idea would be that there is redundancy and it is ok for you to clear your cache and storage. As for the hoster, they would probably use a <em>nodejs</em> type server to host their files instead of relying on having a browser tab open.<p>However, I do agree that relying on regular web browsers is not a good idea. A browser update could limit resources of background tabs even more than they normally do and soon your application is down.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Static torrent website with peer-to-peer queries over BitTorrent on 2M records",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://boredcaveman.xyz/post/0x2_static-torrent-website-p2p-queries.html",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T14:06:18.000Z",
            "title": null,
            "url": null,
            "author": "jhonnyzao",
            "points": null,
            "story_text": null,
            "comment_text": "<p><pre><code>  Location: São Paulo, Brazil.\n  Remote: Yes.\n  Willing to relocate: No.\n  Technologies: Node,js, C#&#x2F;.NET, PHP, React, AWS Stack, MySQL, SQLServer, Redis, Git, SCRUM.\n  Résumé&#x2F;CV: https:&#x2F;&#x2F;bit.ly&#x2F;3pSAMqe (Linkedin: https:&#x2F;&#x2F;www.linkedin.com&#x2F;in&#x2F;jo%C3%A3o-armando-moretti-ferreira-30114a99&#x2F;)\n  Email: joaoarmando94@gmail.com</code></pre>\nHello! I&#x27;m a Software Developer with a focus on back-end currently working for the biggest investments bank in Latin America. I have 8+ years of experience and I&#x27;m a hands-on and a fast learner, looking for a healthy work environment.",
            "num_comments": null,
            "story_id": 30515748,
            "story_title": "Ask HN: Who wants to be hired? (March 2022)",
            "story_url": null,
            "parent_id": 30515748,
            "created_at_i": 1646834778,
            "_tags": [
                "comment",
                "author_jhonnyzao",
                "story_30515748"
            ],
            "objectID": "30614491",
            "_highlightResult": {
                "author": {
                    "value": "jhonnyzao",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "<p><pre><code>  Location: São Paulo, Brazil.\n  Remote: Yes.\n  Willing to relocate: No.\n  Technologies: <em>Node,js</em>, C#/.NET, PHP, React, AWS Stack, MySQL, SQLServer, Redis, Git, SCRUM.\n  Résumé/CV: https://bit.ly/3pSAMqe (Linkedin: https://www.linkedin.com/in/jo%C3%A3o-armando-moretti-ferreira-30114a99/)\n  Email: joaoarmando94@gmail.com</code></pre>\nHello! I'm a Software Developer with a focus on back-end currently working for the biggest investments bank in Latin America. I have 8+ years of experience and I'm a hands-on and a fast learner, looking for a healthy work environment.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Ask HN: Who wants to be hired? (March 2022)",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T13:03:03.000Z",
            "title": "A Complete Guide to Node.js Process Management with PM2",
            "url": "https://blog.appsignal.com/2022/03/09/a-complete-guide-to-nodejs-process-management-with-pm2.html",
            "author": "Liriel",
            "points": 1,
            "story_text": null,
            "comment_text": null,
            "num_comments": 0,
            "story_id": null,
            "story_title": null,
            "story_url": null,
            "parent_id": null,
            "created_at_i": 1646830983,
            "_tags": [
                "story",
                "author_Liriel",
                "story_30613959"
            ],
            "objectID": "30613959",
            "_highlightResult": {
                "title": {
                    "value": "A Complete Guide to Node.js Process Management with PM2",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "url": {
                    "value": "https://blog.appsignal.com/2022/03/09/a-complete-guide-to-<em>nodejs</em>-process-management-with-pm2.html",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "author": {
                    "value": "Liriel",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T12:44:50.000Z",
            "title": null,
            "url": null,
            "author": "Curious_user1",
            "points": null,
            "story_text": null,
            "comment_text": "I have written notes for nextjs for people with primary understanding in React and Nodejs.",
            "num_comments": null,
            "story_id": 30613821,
            "story_title": "Notes for Nextjs Written in Markdown GitHub",
            "story_url": "https://github.com/Serjeel-Ranjan-911/NextJSNotes",
            "parent_id": 30613821,
            "created_at_i": 1646829890,
            "_tags": [
                "comment",
                "author_Curious_user1",
                "story_30613821"
            ],
            "objectID": "30613822",
            "_highlightResult": {
                "author": {
                    "value": "Curious_user1",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "I have written notes for nextjs for people with primary understanding in React and <em>Nodejs</em>.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Notes for Nextjs Written in Markdown GitHub",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://github.com/Serjeel-Ranjan-911/NextJSNotes",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T09:59:40.000Z",
            "title": null,
            "url": null,
            "author": "dmurray",
            "points": null,
            "story_text": null,
            "comment_text": "&gt; 2. Node.js is not a language. JavaScript is a language,<p>This criticism is the wrong way around. All of the author&#x27;s &quot;languages&quot; are actually <i>language implementations</i> like NodeJS. You can tell because he produced the results by running the code, rather than by reading a spec.",
            "num_comments": null,
            "story_id": 30611367,
            "story_title": "Bugs in Hello World",
            "story_url": "https://blog.sunfishcode.online/bugs-in-hello-world/",
            "parent_id": 30612205,
            "created_at_i": 1646819980,
            "_tags": [
                "comment",
                "author_dmurray",
                "story_30611367"
            ],
            "objectID": "30612764",
            "_highlightResult": {
                "author": {
                    "value": "dmurray",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "&gt; 2. Node.js is not a language. JavaScript is a language,<p>This criticism is the wrong way around. All of the author's &quot;languages&quot; are actually <i>language implementations</i> like <em>NodeJS</em>. You can tell because he produced the results by running the code, rather than by reading a spec.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "Bugs in Hello World",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://blog.sunfishcode.online/bugs-in-hello-world/",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-09T03:29:22.000Z",
            "title": null,
            "url": null,
            "author": "truth_seeker",
            "points": null,
            "story_text": null,
            "comment_text": "how about using plain v8 (much faster in execution of JS code) without embedding it in NodeJS ?<p><a href=\"https:&#x2F;&#x2F;github.com&#x2F;just-js\" rel=\"nofollow\">https:&#x2F;&#x2F;github.com&#x2F;just-js</a>",
            "num_comments": null,
            "story_id": 30598026,
            "story_title": "QuickJS JavaScript Engine",
            "story_url": "https://bellard.org/quickjs/",
            "parent_id": 30598026,
            "created_at_i": 1646796562,
            "_tags": [
                "comment",
                "author_truth_seeker",
                "story_30598026"
            ],
            "objectID": "30610579",
            "_highlightResult": {
                "author": {
                    "value": "truth_seeker",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "how about using plain v8 (much faster in execution of JS code) without embedding it in <em>NodeJS</em> ?<p><a href=\"https://github.com/just-js\" rel=\"nofollow\">https://github.com/just-js</a>",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "QuickJS JavaScript Engine",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://bellard.org/quickjs/",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-08T16:54:35.000Z",
            "title": null,
            "url": null,
            "author": "jitl",
            "points": null,
            "story_text": null,
            "comment_text": "If you want to use QuickJS in the browser and&#x2F;or NodeJS for running untrusted code, building plugin systems, etc, I have a library here that wraps a WASM build of QuickJS:<p><a href=\"https:&#x2F;&#x2F;github.com&#x2F;justjake&#x2F;quickjs-emscripten#quickjs-emscripten\" rel=\"nofollow\">https:&#x2F;&#x2F;github.com&#x2F;justjake&#x2F;quickjs-emscripten#quickjs-emscr...</a><p>I believe Figma uses QuickJS for their plugin system.",
            "num_comments": null,
            "story_id": 30598026,
            "story_title": "QuickJS JavaScript Engine",
            "story_url": "https://bellard.org/quickjs/",
            "parent_id": 30598026,
            "created_at_i": 1646758475,
            "_tags": [
                "comment",
                "author_jitl",
                "story_30598026"
            ],
            "objectID": "30602178",
            "_highlightResult": {
                "author": {
                    "value": "jitl",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "If you want to use QuickJS in the browser and/or <em>NodeJS</em> for running untrusted code, building plugin systems, etc, I have a library here that wraps a WASM build of QuickJS:<p><a href=\"https://github.com/justjake/quickjs-emscripten#quickjs-emscripten\" rel=\"nofollow\">https://github.com/justjake/quickjs-emscripten#quickjs-emscr...</a><p>I believe Figma uses QuickJS for their plugin system.",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                },
                "story_title": {
                    "value": "QuickJS JavaScript Engine",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "story_url": {
                    "value": "https://bellard.org/quickjs/",
                    "matchLevel": "none",
                    "matchedWords": []
                }
            }
        },
        {
            "created_at": "2022-03-08T15:39:03.000Z",
            "title": null,
            "url": null,
            "author": "TechRecruiting",
            "points": null,
            "story_text": null,
            "comment_text": "We are currently looking for an experienced and passionate Node.js Developer (m&#x2F;f&#x2F;d) to join our Customer Relationship Management (CRM) Team!<p>Our CRM Team acts as an interface between various cross-functional teams with the purpose to inspire our customers by intelligent mailing- and push notifications. Data-driven, creative and with high speed, the Team is building new features for our CRM systems to extend our diverse communication channels and make them even more efficient. It reaches more than one million customers per day via different channels and plays a crucial role in strengthening our customer loyalty.<p>Challenges the team will be working on in the next 12 months:<p>Improve &amp; develop our CRM Tool in NodeJS<p>Build a tool that is used on a daily basis, reaches a big audience and is a customer-facing product<p>What you will do\nDevelop &amp; implement new features for our customer-facing CRM in Node.js using TypeScript<p>Handle big amount of customer data in MySQL &amp; Google BigQuery and thereby provide personalized campaigns (on site, mailing and push notifications)\nCreate API’s to different touch points (e.g. our frontend shop applications or the checkout)\nCollaborate with internal stakeholders like our Shop Applications Team, Online Marketing &amp; Business Intelligence\nWork in an agile environment and possibility to put in own ideas in a short decision-making process\nWho you are\nPrevious professional experience in Node.js development (min. 2 years)<p>Good knowledge in TypeScript \nProfound knowledge in handling big data in SQL (preferably MySQL) and Google BigQuery\nSolution-oriented and KPI-driven working style\nVery good written and spoken English skills\nNice to have\nExperience in Node.js frameworks (e.g. Nest.js, LoopBack or Sails.js)<p>Frontend experience with e.g. React.js, Angular or VueJs\nExperience in storing data in Redis<p>YOU ARE THE CORE OF ABOUT YOU. \nWe take responsibility for creating an inclusive and exceptional environment where all genders, nationalities and ethnicities feel welcomed and accepted exactly as they are. We believe that a diverse workforce essentially contributes to the ABOUT YOU culture. In order to maintain talent and diversity, we emphasize the care for physical health, mental health and overall well-being. Our values and work ethics essentially contribute to our brand mission: empower acceptance and shape an inclusive, fair and circular fashion culture.<p>We are looking forward to receiving your application – preferably via our online application portal! Thus, we can ensure a faster process and for you it is very easy to upload your application documents. :-)",
            "num_comments": null,
            "story_id": 30601038,
            "story_title": null,
            "story_url": null,
            "parent_id": 30601038,
            "created_at_i": 1646753943,
            "_tags": [
                "comment",
                "author_TechRecruiting",
                "story_30601038"
            ],
            "objectID": "30601039",
            "_highlightResult": {
                "author": {
                    "value": "TechRecruiting",
                    "matchLevel": "none",
                    "matchedWords": []
                },
                "comment_text": {
                    "value": "We are currently looking for an experienced and passionate Node.js Developer (m/f/d) to join our Customer Relationship Management (CRM) Team!<p>Our CRM Team acts as an interface between various cross-functional teams with the purpose to inspire our customers by intelligent mailing- and push notifications. Data-driven, creative and with high speed, the Team is building new features for our CRM systems to extend our diverse communication channels and make them even more efficient. It reaches more than one million customers per day via different channels and plays a crucial role in strengthening our customer loyalty.<p>Challenges the team will be working on in the next 12 months:<p>Improve &amp; develop our CRM Tool in <em>NodeJS</em><p>Build a tool that is used on a daily basis, reaches a big audience and is a customer-facing product<p>What you will do\nDevelop &amp; implement new features for our customer-facing CRM in Node.js using TypeScript<p>Handle big amount of customer data in MySQL &amp; Google BigQuery and thereby provide personalized campaigns (on site, mailing and push notifications)\nCreate API’s to different touch points (e.g. our frontend shop applications or the checkout)\nCollaborate with internal stakeholders like our Shop Applications Team, Online Marketing &amp; Business Intelligence\nWork in an agile environment and possibility to put in own ideas in a short decision-making process\nWho you are\nPrevious professional experience in Node.js development (min. 2 years)<p>Good knowledge in TypeScript \nProfound knowledge in handling big data in SQL (preferably MySQL) and Google BigQuery\nSolution-oriented and KPI-driven working style\nVery good written and spoken English skills\nNice to have\nExperience in Node.js frameworks (e.g. Nest.js, LoopBack or Sails.js)<p>Frontend experience with e.g. React.js, Angular or VueJs\nExperience in storing data in Redis<p>YOU ARE THE CORE OF ABOUT YOU. \nWe take responsibility for creating an inclusive and exceptional environment where all genders, nationalities and ethnicities feel welcomed and accepted exactly as they are. We believe that a diverse workforce essentially contributes to the ABOUT YOU culture. In order to maintain talent and diversity, we emphasize the care for physical health, mental health and overall well-being. Our values and work ethics essentially contribute to our brand mission: empower acceptance and shape an inclusive, fair and circular fashion culture.<p>We are looking forward to receiving your application – preferably via our online application portal! Thus, we can ensure a faster process and for you it is very easy to upload your application documents. :-)",
                    "matchLevel": "full",
                    "fullyHighlighted": false,
                    "matchedWords": [
                        "nodejs"
                    ]
                }
            }
        }
    ],
    "nbHits": 22426,
    "page": 0,
    "nbPages": 50,
    "hitsPerPage": 20,
    "exhaustiveNbHits": true,
    "exhaustiveTypo": true,
    "query": "nodejs",
    "params": "advancedSyntax=true&analytics=true&analyticsTags=backend&query=nodejs",
    "processingTimeMS": 8
}