import { Injectable } from "@nestjs/common";
import { ICategoriesProvider } from '../categories.provider';
import axios from 'axios';
import { ResponseService } from '../../controller/dto/response-job.dto';
import { resourceLimits } from "worker_threads";
import { CatalogTerminalesgModel } from "../model/catalogterminales.model";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { ICategorie } from "src/core/entity/categorie.entity";
import { ICatalog } from "src/core/entity/catalog.entity";
import { CatalogTecnologiaModel } from '../model/catalogtecnologia.model';
import { CatalogPostpagoModel } from '../model/catalogpostpago.model';
import { CatalogPrepagoModel } from '../model/catalogprepago.model';
import { BusinessException } from '../../common/lib/business-exceptions';
import { EmessageMapping } from '../../common/utils/enums/message.enum';
import generalConfig from 'src/common/configuration/general.config';


@Injectable()
export class CategoriesProvider implements ICategoriesProvider {

    constructor(
        @InjectModel(CatalogTerminalesgModel.name) private readonly catalogTerminalModel: Model<CatalogTerminalesgModel>,
        @InjectModel(CatalogTecnologiaModel.name) private readonly catalogTecnologiaModel: Model<CatalogTecnologiaModel>,
        @InjectModel(CatalogPostpagoModel.name) private readonly catalogPostPagoModel: Model<CatalogPostpagoModel>,
        @InjectModel(CatalogPrepagoModel.name) private readonly catalogPrePagoModel: Model<CatalogPrepagoModel>
    ) { }



    async getCategories(): Promise<ICategorie[]> {
        let categories;

        await axios.get(
            generalConfig.categories
        ).then((result) => {
            // console.log('CATEGORIAS');
            // console.log(result.data)
            categories = result.data.hits
        }).catch((error) => {
            console.log(error);
        })

        //console.log('CATEGORIES ====>', categories)
        return await categories;
    }

    async getCatalogs(name): Promise<any> {
        try {
            let result2;
            const requestBody = {
                "context":
                    [
                        {
                            "name": "onBehalfOf",
                            "value": "upadmin"
                        },
                        {
                            "name": "ContextName",
                            "value": "InspiraGeneralContext"
                        }
                    ]
            };
            const config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }

            await axios.post(
                `${generalConfig.catalogs}${name}`,
                requestBody,
                config)
                .then((result) => {
                    result2 = result.data
                }).catch((error) => {
                    result2 = false
                });
            //console.log('RESULT 2 =>', result2)
            return result2;


        } catch (error) {
            throw new BusinessException(
                500,
                EmessageMapping.INTERNAL_SERVER_ERROR,
                false
            )
        }
    }
   
    //aqui es donde se actualizan las noticias los demas metodos ignorarlos.

    async catalogTerminal(catalog: any) {
        //console.log('JUESTAMENTE AQUI ES DONDE SUCEDE LA MAGIA Y SE GUARDA =>', catalog)

        let consulta = {
            "title": catalog.title,
        }

        await this.catalogTerminalModel.findOneAndUpdate(consulta, catalog, {
            new: true,
            upsert: true,
        });
    }

    //Metodo guardar en DB
    async catalogTecnologiaGeneral(catalog: any) {
        //console.log('PRODUCT =>', catalog)

        let consulta = {
            "partNumber": catalog.partNumber
        }

        await this.catalogTecnologiaModel.findOneAndUpdate(consulta, catalog, {
            new: true,
            upsert: true,
        });
    }


    async posPlaDat(catalog: ICatalog) {
        console.log('PRODUCT =======>', catalog)

        let consulta = {
            "title": catalog.title
        }

        await this.catalogPostPagoModel.findOneAndUpdate(consulta, catalog, {
            new: true,
            upsert: true,
        });
    }


    async posPlaMov(catalog: ICatalog) {

        //console.log('PRODUCT =>', catalog)

        let consulta = {
            "partNumber": catalog.partNumber
        }

        await this.catalogPostPagoModel.findOneAndUpdate(consulta, catalog, {
            new: true,
            upsert: true,
        });
    }


    async prePla(catalog: ICatalog) {

        //console.log('PRODUCT =>', catalog)

        let consulta = {
            "partNumber": catalog.partNumber
        }

        await this.catalogPrePagoModel.findOneAndUpdate(consulta, catalog, {
            new: true,
            upsert: true,
        });
    }

}