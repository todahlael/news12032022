import { Injectable } from "@nestjs/common";
import { ResponseService } from "src/controller/dto/response-job.dto";
import { ICatalog } from "src/core/entity/catalog.entity";
import { ICategorie } from '../core/entity/categorie.entity';


@Injectable()
export abstract class ICategoriesProvider {
    abstract getCategories(): Promise<ICategorie[]>;
    abstract getCatalogs(name: String): Promise<any>;
    abstract catalogTerminal(catalog: any);
    abstract catalogTecnologiaGeneral(catalog: any);
    abstract posPlaMov(catalog: any);
    abstract posPlaDat(catalog: any);
    abstract prePla(catalog: any);

}