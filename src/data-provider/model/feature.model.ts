import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


@Schema()
export class FeatureModel extends Document{ 

    @Prop()
    name: string;

    @Prop()
    value: string;

 
}

export const FeatureSchema = SchemaFactory.createForClass(FeatureModel);