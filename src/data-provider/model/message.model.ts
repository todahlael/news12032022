import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({versionKey: false})
export class MessageModel extends Document{ 

    @Prop()
    hour: string;

    @Prop()
    minute : number;
   
   

}

export const MessageSchema = SchemaFactory.createForClass(MessageModel);