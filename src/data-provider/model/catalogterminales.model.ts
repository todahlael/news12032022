import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
import { FeatureModel } from "./feature.model";


@Schema()
export class CatalogTerminalesgModel extends Document {

    @Prop()
    title: string;

    @Prop()
    url: string;

    @Prop()
    author: string;

    @Prop()
    comment_text: string;

    @Prop()
    parent_id: string;

    @Prop()
    created_at : string;

    @Prop()
    status : string;


}

export const CatalogTerminalesSchema = SchemaFactory.createForClass(CatalogTerminalesgModel);