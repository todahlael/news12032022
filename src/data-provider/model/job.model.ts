import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({versionKey: false})
export class JobModel extends Document{     
    hour: number; 
    minute: number;   

}

export const JobSchema = SchemaFactory.createForClass(JobModel);
