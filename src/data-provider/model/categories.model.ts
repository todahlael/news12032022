import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";


@Schema()
export class CategoriesModel extends Document {

    @Prop()
    state: boolean;

    @Prop()
    name: string;

    @Prop()
    type: string;

}

export const CategoriesSchema = SchemaFactory.createForClass(CategoriesModel);