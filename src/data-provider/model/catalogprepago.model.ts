import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
import { FeatureModel } from "./feature.model";


@Schema()
export class CatalogPrepagoModel extends Document {

    @Prop()
    partNumber: string;

    @Prop()
    features: FeatureModel[];

    @Prop()
    name: string;

    @Prop()
    description: string;

    @Prop()
    id: string;

}

export const CatalogPrepagoSchema = SchemaFactory.createForClass(CatalogPrepagoModel);