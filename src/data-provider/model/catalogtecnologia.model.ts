import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
import { propfind } from "superagent";
import { FeatureModel } from "./feature.model";


@Schema()
export class CatalogTecnologiaModel extends Document {

    @Prop()
    title: string;

    @Prop()
    url: FeatureModel[];

    @Prop()
    author: string;

    @Prop()
    comment_text: string;

    @Prop()
    parent_id: string;

    @Prop()
    created_at : string;

}

export const CatalogTecnologiaSchema = SchemaFactory.createForClass(CatalogTecnologiaModel);