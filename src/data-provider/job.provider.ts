import { Injectable } from '@nestjs/common';
import { IJobDTO } from 'src/controller/dto/jobcron/job.dto';
import { IJob } from 'src/core/entity/job.entity';


@Injectable()
export abstract class IJobProvider {

    abstract updateMessage(id: string): Promise<IJob>;
    abstract getHour(): Promise<IJob>;
    abstract getHourCron();
}