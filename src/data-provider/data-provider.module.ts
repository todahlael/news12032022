import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import databaseConfig from '../common/configuration/database.config';
import { IJobProvider } from './job.provider';
import { JobModel, JobSchema } from './model/job.model';
import { MessageModel, MessageSchema } from './model/message.model';
import { JobProvider } from './provider/job.provider.impl';
import { ICategoriesProvider } from './categories.provider';
import { CategoriesProvider } from './provider/categories.provider.impl';
import { CatalogTerminalesgModel, CatalogTerminalesSchema } from './model/catalogterminales.model';
import { CatalogPostpagoModel, CatalogPostpagoSchema } from './model/catalogpostpago.model';
import { CatalogPrepagoModel, CatalogPrepagoSchema } from './model/catalogprepago.model';
import { CatalogTecnologiaModel, CatalogTecnologiaSchema } from './model/catalogtecnologia.model';


@Module({
  imports: [
    //Conexión a base de datos
    MongooseModule.forRoot(databaseConfig.database, {
      retryAttempts: 3,
      useCreateIndex: true,
      useFindAndModify: false,
      autoCreate: true,
    }),
    MongooseModule.forFeature([
      {
        name: MessageModel.name,
        schema: MessageSchema,
        collection: 'coll_job',
      },
      {
        name: CatalogTerminalesgModel.name,
        schema: CatalogTerminalesSchema,
        collection: 'coll_equipment_catalogue',
      },
      {
        name: CatalogTecnologiaModel.name,
        schema: CatalogTecnologiaSchema,
        collection: 'coll_technology_catalogue',
      },
      {
        name: CatalogPostpagoModel.name,
        schema: CatalogPostpagoSchema,
        collection: 'coll_pospago_catalogue',
      },
      {
        name: CatalogPrepagoModel.name,
        schema: CatalogPrepagoSchema,
        collection: 'coll_prepago_catalogue',
      },
    ])
  ],
  providers: [
    { provide: IJobProvider, useClass: JobProvider },
    { provide: ICategoriesProvider, useClass: CategoriesProvider }
  ],
  exports: [IJobProvider, ICategoriesProvider],
})
export class DataProviderModule { }
