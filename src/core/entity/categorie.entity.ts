export interface ICategorie {
    id?: string;
    state: boolean;
    name: string;
    type: string;
}