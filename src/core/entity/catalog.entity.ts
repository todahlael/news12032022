import { IFeature } from "./feature.entity";

export interface ICatalog {
    title: any;
    id?: string;
    partNumber: string;
    feature: IFeature[];
    name: string;
    description: string;
}