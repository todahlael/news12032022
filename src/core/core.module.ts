import { Module } from '@nestjs/common';
import { DataProviderModule } from 'src/data-provider/data-provider.module';
import { JobUcimpl } from './use-case/impl/job.uc.impl';
import { IJobUc } from './use-case/job.uc';
import { ICategoriesUC } from './use-case/categories.uc';
import { CategoriesUC } from './use-case/impl/categories.uc.impl';


@Module({
  imports: [DataProviderModule],
  providers: [

    { provide: IJobUc, useClass: JobUcimpl },
    { provide: ICategoriesUC, useClass: CategoriesUC }

  ],
  exports: [IJobUc, ICategoriesUC],
})
export class CoreModule { }
