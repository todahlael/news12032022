import { Injectable } from '@nestjs/common';
import { BusinessException } from 'src/common/lib/business-exceptions';
import Logging from 'src/common/lib/logging';
import { IJobUc } from '../job.uc';
import { IJob } from 'src/core/entity/job.entity';
import { IJobProvider } from 'src/data-provider/job.provider';
import { IJobDTO } from 'src/controller/dto/jobcron/job.dto';
import { IMessage } from 'src/core/entity/message.entity';
import { SchedulerRegistry } from '@nestjs/schedule';
import { CronJob } from 'cron';
import { ICategoriesUC } from '../categories.uc';
import { Etask } from 'src/common/utils/enums/taks.enum';




@Injectable()
export class JobUcimpl implements IJobUc {


    private static messages: IMessage[] = [];
    private readonly logger = new Logging(JobUcimpl.name);

    constructor(
        public readonly _ijobProvider: IJobProvider,
        public readonly iCategoriesUc: ICategoriesUC,
        private readonly schedulerRegistr: SchedulerRegistry) {
        // //this.getHourCron();
        // this.iCategoriesUc.getCategories();
        this.organizar()
        //this.addCronJob("Cronjob", '5 * * * * *');
    }


    async addCronJob(name: string, seconds: string) {

        if (!name) {
            console.log('NO CRON')
        }

        try {

            const job = await new CronJob(`${seconds}`, () => {
                this.organizar();
            });

            if (!name) {
                this.schedulerRegistr.addCronJob(name, job);
            }
            //console.log(`Job ${name} added`);
            this.logger.write(`Job ${name} added`, Etask.JOB_ADDED, false);
            job.start();
        } catch (error) {
            console.log(error);
        }

    }

    async organizar() {
        //this.getHourCron();
        let categories = await this.iCategoriesUc.getCategories();
        categories ? this.getHourCron() : false


    }



    public static get getMessages(): IMessage[] {
        return JobUcimpl.messages;
    }



    async update(id: string): Promise<IJob> {
        const result = await this._ijobProvider.updateMessage(id);
        if (result == null)
            throw new BusinessException(400, 'Bad request', true);
        // Si se actualiza en bd, actualizar variable estática      
        if (!CronJob) {
            this.schedulerRegistr.deleteCronJob('Cronjob');
        }
        this.getHourCron();
        return result;
    }

    async getHour(): Promise<any> {
        const result = await this._ijobProvider.getHour();
        if (result == null)
            throw new BusinessException(400, 'Bad request', true);
        console.log(result);
        return result

    }

    async getHourCron() {
        const hour = await this._ijobProvider.getHourCron();
        await this.addCronJob("Cronjob", hour);
        //console.log('HS CRON DESDE DB =>', hour);
        return hour;
    }
}