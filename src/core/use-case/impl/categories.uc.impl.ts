import { Injectable } from "@nestjs/common";
import { ICategoriesUC } from "../categories.uc";
import { ICategoriesProvider } from '../../../data-provider/categories.provider';
import { ResponseService } from "src/controller/dto/response-job.dto";
import { ICategorie } from "src/core/entity/categorie.entity";
import { ICatalog } from "src/core/entity/catalog.entity";
import { BusinessException } from '../../../common/lib/business-exceptions';
import { EmessageMapping } from '../../../common/utils/enums/message.enum';
import Logging from "src/common/lib/logging";
import { Etask } from "src/common/utils/enums/taks.enum";
import { PosPlaDat, PosPlaMov, PrePla, TecnologiaGeneral, TerminalesGeneral } from "src/common/utils/enums/features.enum";
import { title } from "process";


@Injectable()
export class CategoriesUC implements ICategoriesUC {



    private readonly logger = new Logging(CategoriesUC.name);

    constructor(public readonly iCategoriesProvider: ICategoriesProvider) { }

    async getCategories(): Promise<ResponseService<ICategorie[]>> {
        console.log("EL PROCESO DE EJECUTACION EMPIEZA AQUI")
        const categories: any = await this.callCategory();
        await this.getCatalogsNews(categories);
        return new ResponseService(
            true,
            'Execution Successful',
            200,
            categories
        );
    }

    callCategory(): Promise<ICategorie[]> {

        return this.iCategoriesProvider.getCategories();
    }

    async getCatalogsNews(categories: any): Promise<any> {

        try {
            let categoryArr = categories
            console.log("aqui esta donde va a desectrucutra", categoryArr)
            let contador = categoryArr.length
            //console.log('CONTADOR =>', contador);


            for (let index = 0; index < contador; index++) {
                console.log(index)
                let titleNews = categoryArr[index].story_title;
                if (titleNews == null) {
                    titleNews = categoryArr[index].title;
                }
                console.log(titleNews)

                let final2 = {
                    "title": titleNews,
                    "url": categoryArr[index].url,
                    "author": categoryArr[index].author,
                    "comment_text": categoryArr[index].comment_text,
                    "parent_id" : categoryArr[index].parent_id,
                    "created_at" : categoryArr[index].created_at,
                }


                this.iCategoriesProvider.catalogTerminal(final2);



            }


        } catch (error) {
            throw error;
        }
    }
}