import { Injectable } from "@nestjs/common";
import { ResponseService } from "src/controller/dto/response-service.dto";
import { ICategorie } from "../entity/categorie.entity";




@Injectable()
export abstract class ICategoriesUC {
    abstract getCategories(): Promise<ResponseService<ICategorie[]>>;
}