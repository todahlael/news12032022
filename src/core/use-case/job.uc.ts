import { Injectable, OnModuleInit } from '@nestjs/common';
import { StringifyOptions } from 'querystring';
import { IJobDTO } from 'src/controller/dto/jobcron/job.dto';


@Injectable()
export abstract class IJobUc {

    abstract update(id: string): Promise<any>;
    abstract getHour(): Promise<any>;
    abstract getHourCron();

}